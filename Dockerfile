FROM openjdk:15-jdk-buster

RUN set -x \
 && curl -Lo /ngrok.zip https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip \
 && unzip -o /ngrok.zip -d /usr/local/bin/ \
 && rm -f /ngrok.zip

#Copy the jar to the image
COPY target/*.jar /opt/assistant/ta-tunnel/
COPY target/classes/ngrok.yml  /home/ngrok/.ngrok2/ngrok.yml

#Make it executable
RUN chmod u+x /opt/assistant/ta-tunnel/

#Set the startup script to be run on startup ngrok http 80
ENTRYPOINT java -jar /opt/assistant/ta-tunnel/*.jar


