package com.assistant.tunnel.integration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.amqp.outbound.AmqpOutboundEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.json.ObjectToJsonTransformer;
import org.springframework.messaging.MessageChannel;

import static org.apache.commons.lang3.StringUtils.EMPTY;

@Slf4j
@Configuration
public class RabbitMQOutboundConfig {

    private static final String TUNNEL_EVENT_EXCHANGE = "TUNNEL_EVENT_EXCHANGE";
    private static final String TUNNEL_EVENT_QUEUE = "TUNNEL_EVENT_QUEUE";

    @Bean
    public MessageChannel tunnelEventOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    @Transformer(inputChannel = "tunnelEventOutboundChannel", outputChannel = "amqpOutboundChannel")
    public ObjectToJsonTransformer objectToJsonTransformer() {
        return new ObjectToJsonTransformer();
    }

    @Bean
    public MessageChannel amqpOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    public Queue tunnelEventQueue() {
        return new Queue(TUNNEL_EVENT_QUEUE, false);
    }

    @Bean
    public DirectExchange tunnelEventExchange() {
        return ExchangeBuilder.directExchange(TUNNEL_EVENT_EXCHANGE).build();
    }

    @Bean
    public Binding bindingTunnelEventExchange(Queue tunnelEventQueue, DirectExchange tunnelEventExchange) {
        return BindingBuilder.bind(tunnelEventQueue).to(tunnelEventExchange).with(EMPTY);
    }

    @Bean
    @ServiceActivator(inputChannel = "amqpOutboundChannel")
    public AmqpOutboundEndpoint amqpOutbound(AmqpTemplate amqpTemplate, DirectExchange tunnelEventExchange) {
        AmqpOutboundEndpoint amqpOutboundEndpoint = new AmqpOutboundEndpoint(amqpTemplate);
        amqpOutboundEndpoint.setExpectReply(false);
        amqpOutboundEndpoint.setExchangeName(tunnelEventExchange.getName());
        return amqpOutboundEndpoint;
    }

}
