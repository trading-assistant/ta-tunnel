package com.assistant.tunnel.integration.gateway;

import com.assistant.commons.tunnel.TunnelEvent;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway
public interface TunnelEventPublishGateway {

    @Gateway(requestChannel = "tunnelEventOutboundChannel")
    void publishTunnelEvent(Message<TunnelEvent> message);

}
