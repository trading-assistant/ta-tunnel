package com.assistant.tunnel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableScheduling
public class TaskExecutorConfiguration {

    @Bean(name = "singleThreadExecutorService")
    @Scope("prototype")
    public TaskExecutor singleThreadExecutorService() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

        executor.setCorePoolSize(1);
        executor.setMaxPoolSize(1);
        executor.setThreadNamePrefix("single_task_executor_thread");
        executor.initialize();

        return executor;
    }

}
