package com.assistant.tunnel.monitoring.helth;

import com.assistant.tunnel.domain.ngrok.process.NgrokProcess;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokProcessHealthIndicator implements HealthIndicator {

    NgrokProcess ngrokProcess;

    @Override
    public Health health() {
        Status status = ngrokProcess.isAlive() ? Status.UP : Status.DOWN;
        return Health.status(status).build();
    }


}
