package com.assistant.tunnel.monitoring.info;

import com.assistant.tunnel.domain.ngrok.tunnel.serice.TunnelService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TunnelInfoContributor implements InfoContributor {

    TunnelService tunnelService;

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("tunnels", tunnelService.getTunnels());
    }

}
