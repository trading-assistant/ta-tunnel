package com.assistant.tunnel.domain.ngrok.tunnel.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Component
@Validated
@ConfigurationProperties(prefix = "tunnel")
public class TunnelProperties {

    @Value("${tunnel.name-prefix:default}")
    String namePrefix;

    @Value("${tunnel.protocol:http}")
    String protocol;

    @Value("${tunnel.bind-tls:both}")
    String useTLS;

    @NotNull
    @Value("${tunnel.host:localhost}")
    String host;

    @NotNull
    @Min(value = 1)
    @Max(value = 65535)
    @Value("${tunnel.port:8080}")
    Integer port;

}
