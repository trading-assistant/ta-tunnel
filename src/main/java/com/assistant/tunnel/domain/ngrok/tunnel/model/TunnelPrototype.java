package com.assistant.tunnel.domain.ngrok.tunnel.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@ToString
@Builder
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TunnelPrototype {

    @JsonProperty("addr")
    String address;

    @JsonProperty("proto")
    String protocol;

    @JsonProperty("name")
    String name;

    @JsonProperty("bind_tls")
    String useTLS;

}
