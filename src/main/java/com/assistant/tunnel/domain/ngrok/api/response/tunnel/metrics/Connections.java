package com.assistant.tunnel.domain.ngrok.api.response.tunnel.metrics;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Setter
@Getter
@FieldDefaults(level = PRIVATE)
public class Connections {

    Long count;
    Long gauge;
    Long rate1;
    Long rate5;
    Long rate15;
    Long p50;
    Long p90;
    Long p95;
    Long p99;

}
