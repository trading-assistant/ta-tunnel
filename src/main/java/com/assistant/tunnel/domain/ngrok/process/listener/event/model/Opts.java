package com.assistant.tunnel.domain.ngrok.process.listener.event.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Hostname",
        "Auth",
        "Subdomain",
        "HostHeaderRewrite",
        "LocalURLScheme"
})
public class Opts {

    @JsonProperty("Hostname")
    private String hostname;
    @JsonProperty("Auth")
    private String auth;
    @JsonProperty("Subdomain")
    private String subdomain;
    @JsonProperty("HostHeaderRewrite")
    private Boolean hostHeaderRewrite;
    @JsonProperty("LocalURLScheme")
    private String localURLScheme;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Hostname")
    public String getHostname() {
        return hostname;
    }

    @JsonProperty("Hostname")
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @JsonProperty("Auth")
    public String getAuth() {
        return auth;
    }

    @JsonProperty("Auth")
    public void setAuth(String auth) {
        this.auth = auth;
    }

    @JsonProperty("Subdomain")
    public String getSubdomain() {
        return subdomain;
    }

    @JsonProperty("Subdomain")
    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    @JsonProperty("HostHeaderRewrite")
    public Boolean getHostHeaderRewrite() {
        return hostHeaderRewrite;
    }

    @JsonProperty("HostHeaderRewrite")
    public void setHostHeaderRewrite(Boolean hostHeaderRewrite) {
        this.hostHeaderRewrite = hostHeaderRewrite;
    }

    @JsonProperty("LocalURLScheme")
    public String getLocalURLScheme() {
        return localURLScheme;
    }

    @JsonProperty("LocalURLScheme")
    public void setLocalURLScheme(String localURLScheme) {
        this.localURLScheme = localURLScheme;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
