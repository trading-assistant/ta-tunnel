package com.assistant.tunnel.domain.ngrok.api.response.tunnel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@ToString
@Setter
@Getter
@FieldDefaults(level = PRIVATE)
public class NgrokTunnel {

    @JsonProperty("id")
    String id;

    @JsonProperty("name")
    String name;

    @JsonProperty("uri")
    String uri;

    @JsonProperty("public_url")
    String publicUrl;

    @JsonProperty("proto")
    String proto;

    @JsonProperty("config")
    Config config;

    @JsonProperty("metrics")
    Metrics metrics;

}
