package com.assistant.tunnel.domain.ngrok.process.listener.parser;

import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;
import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokKeyValueEvent;
import com.beust.jcommander.JCommander;
import io.vavr.control.Try;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.ArrayUtils.EMPTY_STRING_ARRAY;

@Component
@ConditionalOnProperty(prefix = "ngrok", name = "log-format", havingValue = "KEY_VALUE")
public class KeyValueEventParser implements EventParserStrategy {

    @Override
    public NgrokEvent parse(String eventLine) {
        final NgrokKeyValueEvent event = new NgrokKeyValueEvent();

        final String[] args = getArguments(eventLine);

        Try.of(JCommander::newBuilder)
                .mapTry(builder -> builder.addObject(event))
                .mapTry(JCommander.Builder::build)
                .andThenTry(js -> js.parse(args));

        return event;
    }

    private String[] getArguments(final String finalLine) {
        return Try.of(() -> finalLine)
                .mapTry(CommandLineUtils::translateCommandline)
                .getOrElse(EMPTY_STRING_ARRAY);
    }
}
