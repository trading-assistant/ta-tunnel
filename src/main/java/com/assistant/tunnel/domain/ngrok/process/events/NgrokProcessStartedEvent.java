package com.assistant.tunnel.domain.ngrok.process.events;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.time.Instant;

import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@ToString
@Getter
@Setter
@Builder
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokProcessStartedEvent {

    long pid;
    String user;
    String command;
    String commandLine;
    List<String> arguments;
    Instant started;

    public static NgrokProcessStartedEvent from(Process process) {
        ProcessHandle.Info info = process.info();
        return NgrokProcessStartedEvent.builder()
                .pid(process.pid())
                .user(info.user().orElse(EMPTY))
                .arguments(info.arguments().map(List::of).orElse(List.empty()))
                .started(info.startInstant().orElse(Instant.now()))
                .command(info.command().orElse(EMPTY))
                .commandLine(info.commandLine().orElse(EMPTY))
                .build();
    }

}
