package com.assistant.tunnel.domain.ngrok.process.listener.event;

import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Opts;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Request;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@ToString
@FieldDefaults(level = PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NgrokJsonEvent extends NgrokEvent {


    @Override
    @JsonProperty("dur")
    public String getDur() {
        return super.getDur();
    }

    @Override
    @JsonProperty("pg")
    public String getPg() {
        return super.getPg();
    }

    @Override
    @JsonProperty("dur")
    public void setDur(String dur) {
        super.setDur(dur);
    }

    @Override
    @JsonProperty("pg")
    public void setPg(String pg) {
        super.setPg(pg);
    }

    @Override
    @JsonProperty("t")
    public String getTimestamp() {
        return super.getTimestamp();
    }

    @Override
    @JsonProperty("lvl")
    public String getLvl() {
        return super.getLvl();
    }

    @Override
    @JsonProperty("msg")
    public String getMsg() {
        return super.getMsg();
    }

    @Override
    @JsonProperty("interval")
    public String getInterval() {
        return super.getInterval();
    }

    @Override
    @JsonProperty("reqtype")
    public Integer getReqtype() {
        return super.getReqtype();
    }

    @Override
    @JsonProperty("obj")
    public String getObj() {
        return super.getObj();
    }

    @Override
    @JsonProperty("id")
    public String getId() {
        return super.getId();
    }

    @Override
    @JsonProperty("clientid")
    public String getClientid() {
        return super.getClientid();
    }

    @Override
    @JsonProperty("latency_ms")
    public String getLatency() {
        return super.getLatency();
    }

    @Override
    @JsonProperty("secs")
    public Integer getSecs() {
        return super.getSecs();
    }

    @Override
    @JsonProperty("name")
    public String getName() {
        return super.getName();
    }

    @Override
    @JsonProperty("addr")
    public String getAddr() {
        return super.getAddr();
    }

    @Override
    @JsonProperty("url")
    public String getUrl() {
        return super.getUrl();
    }

    @Override
    @JsonProperty("proto")
    public String getProto() {
        return super.getProto();
    }

    @Override
    @JsonProperty("err")
    public String getError() {
        return super.getError();
    }

    @Override
    @JsonProperty("sid")
    public String getSid() {
        return super.getSid();
    }

    @Override
    @JsonProperty("path")
    public String getPath() {
        return super.getPath();
    }

    @Override
    @JsonProperty("ext")
    public String getExt() {
        return super.getExt();
    }

    @Override
    @JsonProperty("req")
    public Request getReq() {
        return super.getReq();
    }

    @Override
    @JsonProperty("resp")
    public Response getResp() {
        return super.getResp();
    }

    @Override
    @JsonProperty("opts")
    public Opts getOpts() {
        return super.getOpts();
    }

    @Override
    @JsonProperty("t")
    public void setTimestamp(String timestamp) {
        super.setTimestamp(timestamp);
    }

    @Override
    @JsonProperty("lvl")
    public void setLvl(String lvl) {
        super.setLvl(lvl);
    }

    @Override
    @JsonProperty("msg")
    public void setMsg(String msg) {
        super.setMsg(msg);
    }

    @Override
    @JsonProperty("interval")
    public void setInterval(String interval) {
        super.setInterval(interval);
    }

    @Override
    @JsonProperty("reqtype")
    public void setReqtype(Integer reqtype) {
        super.setReqtype(reqtype);
    }

    @Override
    @JsonProperty("obj")
    public void setObj(String obj) {
        super.setObj(obj);
    }

    @Override
    @JsonProperty("id")
    public void setId(String id) {
        super.setId(id);
    }

    @Override
    @JsonProperty("clientid")
    public void setClientid(String clientid) {
        super.setClientid(clientid);
    }

    @Override
    @JsonProperty("latency_ms")
    public void setLatency(String latency) {
        super.setLatency(latency);
    }

    @Override
    @JsonProperty("secs")
    public void setSecs(Integer secs) {
        super.setSecs(secs);
    }

    @Override
    @JsonProperty("name")
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    @JsonProperty("addr")
    public void setAddr(String addr) {
        super.setAddr(addr);
    }

    @Override
    @JsonProperty("url")
    public void setUrl(String url) {
        super.setUrl(url);
    }

    @Override
    @JsonProperty("proto")
    public void setProto(String proto) {
        super.setProto(proto);
    }

    @Override
    @JsonProperty("err")
    public void setError(String error) {
        super.setError(error);
    }

    @Override
    @JsonProperty("sid")
    public void setSid(String sid) {
        super.setSid(sid);
    }

    @Override
    @JsonProperty("path")
    public void setPath(String path) {
        super.setPath(path);
    }

    @Override
    @JsonProperty("ext")
    public void setExt(String ext) {
        super.setExt(ext);
    }

    @Override
    @JsonProperty("req")
    public void setReq(Request req) {
        super.setReq(req);
    }

    @Override
    @JsonProperty("resp")
    public void setResp(Response resp) {
        super.setResp(resp);
    }

    @Override
    @JsonProperty("opts")
    public void setOpts(Opts opts) {
        super.setOpts(opts);
    }


}
