package com.assistant.tunnel.domain.ngrok.tunnel.model;

import com.assistant.tunnel.domain.ngrok.api.response.tunnel.NgrokTunnel;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@ToString
@Setter
@Getter
@Builder
@FieldDefaults(level = PRIVATE)
public class Tunnel {

    @JsonProperty("name")
    String name;

    @JsonProperty("url")
    String url;

    @JsonProperty("address")
    String address;

    @JsonProperty("protocol")
    String protocol;

    public static Tunnel from(NgrokTunnel tunnel) {
        return Tunnel.builder()
                .name(tunnel.getName())
                .url(tunnel.getPublicUrl())
                .protocol(tunnel.getProto())
                .address(tunnel.getConfig().getAddress())
                .build();
    }

}
