package com.assistant.tunnel.domain.ngrok.tunnel.model;


import com.assistant.tunnel.domain.ngrok.tunnel.config.TunnelProperties;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TunnelPrototypeFactory {

    private static final int MAX_TUNNEL_COUNT = 4;

    private AtomicInteger createdTunnelsCount = new AtomicInteger(0);

    TunnelProperties tunnelProperties;

    public TunnelPrototype createTunnelPrototype() {
        int portIncrement = createdTunnelsCount.intValue();
        int tunnelIndex = createdTunnelsCount.incrementAndGet();

        if (tunnelIndex == MAX_TUNNEL_COUNT) {
            this.createdTunnelsCount.set(0);
            return createTunnelPrototype();
        }

        return TunnelPrototype
                .builder()
                .name(tunnelProperties.getNamePrefix() + tunnelIndex)
                .address(tunnelProperties.getHost() + ":" + (tunnelProperties.getPort() + portIncrement) + "")
                .protocol(tunnelProperties.getProtocol())
                .useTLS(tunnelProperties.getUseTLS())
                .build();
    }

}
