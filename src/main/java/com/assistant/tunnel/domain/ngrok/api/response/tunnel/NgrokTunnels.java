package com.assistant.tunnel.domain.ngrok.api.response.tunnel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Setter
@Getter
@FieldDefaults(level = PRIVATE)
public class NgrokTunnels {

    @JsonProperty("tunnels")
    List<NgrokTunnel> tunnels;

    @JsonProperty("uri")
    String uri;

}
