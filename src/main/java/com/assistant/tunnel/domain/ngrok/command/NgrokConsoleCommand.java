package com.assistant.tunnel.domain.ngrok.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokConsoleCommand implements Command {

    private static final String NGROK_SERVICE = "ngrok";

    @Getter
    List<String> arguments;

    public static Command ofKind(NgrokCommand command) {
        return new NgrokConsoleCommand(newArrayList(NGROK_SERVICE, command.getValue()));
    }

    @Override
    public Command addArgument(NgrokCommandArgument argument, String argumentValue) {
        arguments.add(argument.getKey());
        arguments.add(argumentValue);
        return this;
    }

    @Override
    public Command addKeyValueArgument(NgrokCommandArgument argument, String argumentValue) {
        arguments.add(format("%s=%s", argument.getKey(), argumentValue));
        return this;
    }

    @Override
    public Command addArgumentValue(String argumentValue) {
        arguments.add(argumentValue);
        return this;
    }

    @Override
    public Command addArgument(NgrokCommandArgument argument) {
        arguments.add(argument.getKey());
        return this;
    }

}
