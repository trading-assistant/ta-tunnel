package com.assistant.tunnel.domain.ngrok.api;

import com.assistant.tunnel.domain.ngrok.api.response.tunnel.NgrokTunnel;
import com.assistant.tunnel.domain.ngrok.tunnel.model.TunnelPrototype;

import java.util.Collection;

public interface NgrokApiClient {

    Collection<NgrokTunnel> getTunnels();

    boolean isExists(NgrokTunnel tunnel);

    NgrokTunnel startTunnel(TunnelPrototype tunnelPrototype);

    boolean stopTunnel(NgrokTunnel tunnel);

    boolean stopTunnels(Collection<NgrokTunnel> tunnels);

    boolean stopAllTunnels();
}
