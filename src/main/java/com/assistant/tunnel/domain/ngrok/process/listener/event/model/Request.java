package com.assistant.tunnel.domain.ngrok.process.listener.event.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Id",
        "Proto",
        "Opts",
        "Extra"
})
public class Request {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("Proto")
    private String proto;
    @JsonProperty("Opts")
    private Opts opts;
    @JsonProperty("Extra")
    private Extra extra;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("Proto")
    public String getProto() {
        return proto;
    }

    @JsonProperty("Proto")
    public void setProto(String proto) {
        this.proto = proto;
    }

    @JsonProperty("Opts")
    public Opts getOpts() {
        return opts;
    }

    @JsonProperty("Opts")
    public void setOpts(Opts opts) {
        this.opts = opts;
    }

    @JsonProperty("Extra")
    public Extra getExtra() {
        return extra;
    }

    @JsonProperty("Extra")
    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}