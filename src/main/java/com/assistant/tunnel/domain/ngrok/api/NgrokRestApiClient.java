package com.assistant.tunnel.domain.ngrok.api;

import com.assistant.tunnel.domain.ngrok.api.config.NgrokIntegrationProperties;
import com.assistant.tunnel.domain.ngrok.api.response.tunnel.NgrokTunnel;
import com.assistant.tunnel.domain.ngrok.api.response.tunnel.NgrokTunnels;
import com.assistant.tunnel.domain.ngrok.tunnel.model.TunnelPrototype;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpMethod.DELETE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokRestApiClient implements NgrokApiClient {

    RestTemplate httpClient;
    NgrokIntegrationProperties properties;

    @Override
    public List<NgrokTunnel> getTunnels() {
        String tunnelsPath = properties.getTunnelsPath();
        ResponseEntity<NgrokTunnels> response = httpClient.getForEntity(tunnelsPath, NgrokTunnels.class);
        return Try.of(() -> response)
                .mapTry(HttpEntity::getBody)
                .mapTry(NgrokTunnels::getTunnels)
                .filterTry(CollectionUtils::isNotEmpty)
                .getOrElse(emptyList());
    }

    @Override
    public boolean isExists(NgrokTunnel tunnel) {
        String tunnelName = Optional.ofNullable(tunnel).map(NgrokTunnel::getName).orElse(EMPTY);
        return Try.of(() -> httpClient)
                .mapTry(client -> client.getForEntity(properties.getTunnelPath(tunnelName), NgrokTunnel.class))
                .mapTry(ResponseEntity::getStatusCode)
                .mapTry(HttpStatus::is2xxSuccessful)
                .getOrElse(Boolean.FALSE);
    }

    @Override
    public NgrokTunnel startTunnel(TunnelPrototype tunnelPrototype) {
        return httpClient.postForEntity(properties.getTunnelsPath(), tunnelPrototype, NgrokTunnel.class)
                .getBody();
    }

    @Override
    public boolean stopTunnel(NgrokTunnel tunnel) {
        String tunnelName = Optional.ofNullable(tunnel).map(NgrokTunnel::getName).orElse(EMPTY);
        return httpClient.exchange(properties.getTunnelPath(tunnelName), DELETE, null, String.class)
                .getStatusCode()
                .is2xxSuccessful();
    }

    @Override
    public boolean stopTunnels(Collection<NgrokTunnel> tunnels) {
        return tunnels.stream().allMatch(this::stopTunnel);
    }

    @Override
    public boolean stopAllTunnels() {
        return stopTunnels(getTunnels());
    }

}
