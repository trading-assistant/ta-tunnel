package com.assistant.tunnel.domain.ngrok.process.config.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum LogFormat {

    KEY_VALUE("logfmt"),
    JSON("json"),
    DEFAULT("term");

    String value;

}
