package com.assistant.tunnel.domain.ngrok.tunnel;

import com.assistant.commons.chat.HeaderKey;
import com.assistant.commons.tunnel.TunnelEvent;
import com.assistant.tunnel.domain.ngrok.process.events.NgrokStatus;
import com.assistant.tunnel.domain.ngrok.process.events.NgrokStatusChangedEvent;
import com.assistant.tunnel.domain.ngrok.tunnel.model.Tunnel;
import com.assistant.tunnel.domain.ngrok.tunnel.serice.TunnelService;
import com.assistant.tunnel.integration.gateway.TunnelEventPublishGateway;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.atomic.AtomicReference;

import static com.assistant.commons.tunnel.TunnelEvent.TUNNEL_CREATED;
import static com.assistant.tunnel.domain.ngrok.process.events.NgrokStatus.ONLINE;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TunnelHandler {

    TunnelService tunnelService;
    TunnelEventPublishGateway tunnelEventPublishGateway;
    AtomicReference<NgrokStatus> currentStatus = new AtomicReference<>(NgrokStatus.UNKNOWN);

    @EventListener
    public void processNgrokEvent(NgrokStatusChangedEvent ngrokStatusChangedEvent) {
        NgrokStatus ngrokStatus = ngrokStatusChangedEvent.getNgrokStatus();

        if (isOnline(ngrokStatus) && tunnelService.getTunnels().isEmpty()) {
            Tunnel tunnel = tunnelService.startTunnel();
            Message<TunnelEvent> message = MessageBuilder.withPayload(TUNNEL_CREATED)
                    .setHeader(HeaderKey.WEB_HOOK_ADDRESS.getValue(), tunnel.getUrl())
                    .build();
            tunnelEventPublishGateway.publishTunnelEvent(message);
            log.info("Tunnel " + tunnel + " has been started");
        }

        currentStatus.set(ngrokStatus);
    }

    private boolean isOnline(NgrokStatus ngrokStatus) {
        return currentStatus.get() != ngrokStatus && ngrokStatus == ONLINE;
    }

    @PreDestroy
    private void tearDown() {
        boolean stopped = tunnelService.stopAllTunnels();
        if (stopped) {
            log.info("All tunnels have been stopped");
        } else {
            log.warn("There is an issue during tunnels stopping");
        }
    }
}
