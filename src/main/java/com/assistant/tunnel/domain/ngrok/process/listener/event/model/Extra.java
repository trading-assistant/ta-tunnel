package com.assistant.tunnel.domain.ngrok.process.listener.event.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Token",
        "IPPolicyRef",
        "Metadata"
})
public class Extra {

    @JsonProperty("Token")
    private String token;
    @JsonProperty("IPPolicyRef")
    private String iPPolicyRef;
    @JsonProperty("Metadata")
    private String metadata;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("Token")
    public String getToken() {
        return token;
    }

    @JsonProperty("Token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("IPPolicyRef")
    public String getIPPolicyRef() {
        return iPPolicyRef;
    }

    @JsonProperty("IPPolicyRef")
    public void setIPPolicyRef(String iPPolicyRef) {
        this.iPPolicyRef = iPPolicyRef;
    }

    @JsonProperty("Metadata")
    public String getMetadata() {
        return metadata;
    }

    @JsonProperty("Metadata")
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
