package com.assistant.tunnel.domain.ngrok.process.listener.event.model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Id",
        "URL",
        "Proto",
        "Opts",
        "Error",
        "Extra"
})
public class Response {

    @JsonProperty("Id")
    private String id;
    @JsonProperty("URL")
    private String uRL;
    @JsonProperty("Proto")
    private String proto;
    @JsonProperty("Opts")
    private Opts opts;
    @JsonProperty("Error")
    private String error;
    @JsonProperty("Extra")
    private Extra extra;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("Id")
    public String getId() {
        return id;
    }

    @JsonProperty("Id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("URL")
    public String getURL() {
        return uRL;
    }

    @JsonProperty("URL")
    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    @JsonProperty("Proto")
    public String getProto() {
        return proto;
    }

    @JsonProperty("Proto")
    public void setProto(String proto) {
        this.proto = proto;
    }

    @JsonProperty("Opts")
    public Opts getOpts() {
        return opts;
    }

    @JsonProperty("Opts")
    public void setOpts(Opts opts) {
        this.opts = opts;
    }

    @JsonProperty("Error")
    public String getError() {
        return error;
    }

    @JsonProperty("Error")
    public void setError(String error) {
        this.error = error;
    }

    @JsonProperty("Extra")
    public Extra getExtra() {
        return extra;
    }

    @JsonProperty("Extra")
    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}