package com.assistant.tunnel.domain.ngrok.process.listener.parser;

import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;
import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokJsonEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;


@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
@ConditionalOnProperty(prefix = "ngrok", name = "log-format", havingValue = "JSON")
public class JsonEventLineParser implements EventParserStrategy {

    @Override
    public NgrokEvent parse(String eventLine) {
        return convertToObject(eventLine, NgrokJsonEvent.class);
    }


    public <T> T convertToObject(String object, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(object, clazz);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

}
