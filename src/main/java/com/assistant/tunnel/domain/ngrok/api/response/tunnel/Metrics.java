package com.assistant.tunnel.domain.ngrok.api.response.tunnel;

import com.assistant.tunnel.domain.ngrok.api.response.tunnel.metrics.Connections;
import com.assistant.tunnel.domain.ngrok.api.response.tunnel.metrics.Http;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Setter
@Getter
@FieldDefaults(level = PRIVATE)
public class Metrics {

    @JsonProperty("conns")
    Connections connections;

    @JsonProperty("http")
    Http http;

}
