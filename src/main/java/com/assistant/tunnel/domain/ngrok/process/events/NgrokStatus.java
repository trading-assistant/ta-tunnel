package com.assistant.tunnel.domain.ngrok.process.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Set;
import java.util.stream.Stream;

import static lombok.AccessLevel.PRIVATE;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum NgrokStatus {

    UNKNOWN(Set.of()),
    START(Set.of("start")),
    OPEN_STREAM(Set.of("open stream")),
    REQUEST(Set.of("encode request")),
    RESPONSE(Set.of("decoded response")),
    TUNNEL_STARTING(Set.of("start tunnel listen")),
    TUNNEL_STARTED(Set.of("started tunnel")),
    END(Set.of("end")),
    SESSION_STARTING(Set.of("starting web service")),
    TUNNEL_SESSION_STARTED(Set.of("tunnel session started")),
    CLIENT_SESSION_ESTABLISHED(Set.of("client session established")),
    ONLINE(Set.of("heartbeat received")),
    OFFLINE(Set.of("heartbeat timeout, terminating session")),
    CONNECTION_LOST(Set.of("session closed, starting reconnect loop")),
    RECONNECTING(Set.of("failed to reconnect session", "sleep before reconnect"));


    Set<String> statusMessages;

    public static NgrokStatus getStatus(final String status) {
        return Stream.of(NgrokStatus.values())
                .filter(statuses -> statuses.getStatusMessages().contains(status))
                .findAny()
                .orElse(UNKNOWN);
    }

}
