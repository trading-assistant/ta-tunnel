package com.assistant.tunnel.domain.ngrok.process.events;

import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;
import io.vavr.control.Try;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static com.assistant.tunnel.domain.ngrok.process.events.NgrokStatus.UNKNOWN;
import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokStatusChangedEvent {

    NgrokStatus ngrokStatus;
    NgrokEvent ngrokEvent;

    public NgrokStatusChangedEvent(NgrokEvent ngrokEvent) {
        this.ngrokEvent = ngrokEvent;
        this.ngrokStatus = Try.of(() -> ngrokEvent)
                .mapTry(NgrokEvent::getMsg)
                .mapTry(NgrokStatus::getStatus)
                .getOrElse(UNKNOWN);
    }

}
