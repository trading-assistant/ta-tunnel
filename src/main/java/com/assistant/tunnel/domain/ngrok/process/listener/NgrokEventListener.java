package com.assistant.tunnel.domain.ngrok.process.listener;

import com.assistant.tunnel.domain.ngrok.process.events.NgrokStatusChangedEvent;
import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;
import com.assistant.tunnel.domain.ngrok.process.listener.parser.EventLineParser;
import io.vavr.control.Try;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;

import static com.assistant.tunnel.domain.ngrok.process.events.NgrokStatus.UNKNOWN;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;


@Slf4j
@Getter
@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokEventListener {

    EventLineParser eventParser;
    TaskExecutor singleThreadExecutorService;
    ApplicationEventPublisher eventPublisher;

    public void startListening(Try<BufferedReader> ngrokLogReader) {
        singleThreadExecutorService.execute(() -> {
            String rawEvent;
            while (nonNull(rawEvent = ngrokLogReader.mapTry(BufferedReader::readLine).getOrNull())) {
                publishEvent(eventParser.parse(rawEvent));
            }
        });
    }

    private void publishEvent(final NgrokEvent event) {
        Try.of(() -> event)
                .mapTry(NgrokStatusChangedEvent::new)
                .filterTry(e -> e.getNgrokStatus() != UNKNOWN)
                .andThenTry(eventPublisher::publishEvent);
    }

}
