package com.assistant.tunnel.domain.ngrok.tunnel.serice;

import com.assistant.tunnel.domain.ngrok.api.NgrokApiClient;
import com.assistant.tunnel.domain.ngrok.api.response.tunnel.NgrokTunnel;
import com.assistant.tunnel.domain.ngrok.tunnel.model.Tunnel;
import com.assistant.tunnel.domain.ngrok.tunnel.model.TunnelPrototype;
import com.assistant.tunnel.domain.ngrok.tunnel.model.TunnelPrototypeFactory;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TunnelService {

    NgrokApiClient ngrokApiClient;
    TunnelPrototypeFactory tunnelPrototypeFactory;

    public List<Tunnel> getTunnels() {
        Collection<NgrokTunnel> tunnels = ngrokApiClient.getTunnels();
        return tunnels.stream().map(Tunnel::from).collect(toList());
    }

    public Tunnel startTunnel() {
        TunnelPrototype tunnelPrototype = tunnelPrototypeFactory.createTunnelPrototype();
        NgrokTunnel tunnel = ngrokApiClient.startTunnel(tunnelPrototype);
        return Tunnel.from(tunnel);
    }

    public boolean stopAllTunnels() {
        return ngrokApiClient.stopAllTunnels();
    }
}
