package com.assistant.tunnel.domain.ngrok.process.listener.parser;

import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;

public interface EventParserStrategy {

    NgrokEvent parse(String eventLine);

}
