package com.assistant.tunnel.domain.ngrok.command;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum NgrokCommand {

    EXPOSE_HTTP("http"),
    EXPOSE_TCP("tcp"),
    START_NGROK("start"),
    INSTALL_AUTH_TOKEN("authtoken");

    String value;
}
