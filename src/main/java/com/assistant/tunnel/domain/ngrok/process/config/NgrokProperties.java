package com.assistant.tunnel.domain.ngrok.process.config;

import com.assistant.tunnel.domain.ngrok.process.config.enums.LogFormat;
import com.assistant.tunnel.domain.ngrok.process.config.enums.LogLevel;
import com.assistant.tunnel.domain.ngrok.process.config.enums.LogTarget;
import com.assistant.tunnel.domain.ngrok.process.config.enums.Region;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@ToString
@Setter
@Getter
@Component
@Validated
@ConfigurationProperties(prefix = "ngrok")
public class NgrokProperties {

    @Value("${ngrok.log-level:DEFAULT}")
    LogLevel logLevel;

    @Value("${ngrok.log-format:DEFAULT}")
    LogFormat logFormat;

    @Value("${ngrok.log-target:DEFAULT}")
    LogTarget logTarget;

    @Value("${ngrok.region:UNITED_STATES}")
    Region region;

}
