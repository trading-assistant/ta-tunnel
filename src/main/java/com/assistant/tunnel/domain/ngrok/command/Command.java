package com.assistant.tunnel.domain.ngrok.command;

import java.util.List;

public interface Command {

    List<String> getArguments();

    Command addArgument(NgrokCommandArgument argument, String argumentValue);

    Command addKeyValueArgument(NgrokCommandArgument argument, String argumentValue);

    Command addArgumentValue(String argumentValue);

    Command addArgument(NgrokCommandArgument argument);

}
