package com.assistant.tunnel.domain.ngrok.process.config;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokPropertiesHandler {

    NgrokProperties ngrokProperties;

    public String getLogLevel() {
        return ngrokProperties.getLogLevel().getValue();
    }

    public String getLogFormat() {
        return ngrokProperties.getLogFormat().getValue();
    }

    public String getLogTarget() {
        return ngrokProperties.getLogTarget().getValue();
    }

    public String getRegion() {
        return ngrokProperties.getRegion().getSign();
    }

}
