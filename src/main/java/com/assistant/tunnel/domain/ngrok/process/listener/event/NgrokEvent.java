package com.assistant.tunnel.domain.ngrok.process.listener.event;

import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Opts;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Request;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Response;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@ToString
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class NgrokEvent {

    String timestamp;
    String lvl;
    String msg;
    String interval;
    Integer reqtype;
    String obj;
    String id;
    String clientid;
    String latency;
    Integer secs;
    String name;
    String addr;
    String url;
    String proto;
    String error;
    String sid;
    String path;
    String ext;
    Request req;
    Response resp;
    Opts opts;
    String dur;
    String pg;


}
