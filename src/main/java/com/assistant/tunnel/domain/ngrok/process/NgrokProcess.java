package com.assistant.tunnel.domain.ngrok.process;

import com.assistant.tunnel.domain.ngrok.command.Command;
import com.assistant.tunnel.domain.ngrok.command.NgrokCommandFactory;
import com.assistant.tunnel.domain.ngrok.process.events.NgrokProcessStartedEvent;
import com.assistant.tunnel.domain.ngrok.process.listener.NgrokEventListener;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import static java.lang.Boolean.FALSE;
import static java.util.Optional.ofNullable;
import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Setter(value = PRIVATE)
@Service
@RequiredArgsConstructor
public class NgrokProcess {

    private Try<Process> process;

    private final NgrokCommandFactory ngrokCommandFactory;
    private final NgrokEventListener ngrokEventListener;

    @EventListener({ApplicationStartedEvent.class})
    public void start() {
        this.process = Try.of(ngrokCommandFactory::getExposeHttpCommand)
                .mapTry(Command::getArguments)
                .mapTry(ProcessBuilder::new)
                .mapTry(ProcessBuilder::start)
                .onSuccess(process -> log.info("NGROK PROCESS STARTED: " + NgrokProcessStartedEvent.from(process)))
                .onFailure(ex -> log.error("Can't start NGROK process " + ex.getMessage()));

        ngrokEventListener.startListening(getLogReader());
    }

    public Try<BufferedReader> getLogReader() {
        return process
                .mapTry(Process::getInputStream)
                .mapTry(InputStreamReader::new)
                .mapTry(BufferedReader::new);
    }

    public boolean isAlive() {
        return ofNullable(process)
                .map(p -> p.mapTry(Process::isAlive).getOrElse(FALSE))
                .orElse(FALSE);
    }

    public void restart() {
        log.warn("NGROK PROCESS RESTART STARTED");
        stop();
        start();
        log.warn("NGROK PROCESS RESTARTED");
    }

    @PreDestroy
    public void stop() {
        process
                .andThenTry(Process::destroy)
                .andThenTry(Process::waitFor)
                .onSuccess(process -> log.warn("NGROK PROCESS STOPPED PID: " + NgrokProcessStartedEvent.from(process)));
    }

}
