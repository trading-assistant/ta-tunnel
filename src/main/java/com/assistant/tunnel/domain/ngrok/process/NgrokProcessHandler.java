package com.assistant.tunnel.domain.ngrok.process;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokProcessHandler {

    NgrokProcess ngrokProcess;

    @Scheduled(initialDelay = 10000, fixedRate = 10000)
    public void keepProcessAlive() {
        if (!ngrokProcess.isAlive()) {
            log.warn("NGROK PROCESS IS NOT ALIVE");
            ngrokProcess.restart();
        }
    }

}
