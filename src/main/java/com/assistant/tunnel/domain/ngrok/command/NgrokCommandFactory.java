package com.assistant.tunnel.domain.ngrok.command;

import com.assistant.tunnel.domain.ngrok.process.config.NgrokPropertiesHandler;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import static com.assistant.tunnel.domain.ngrok.command.NgrokCommandArgument.*;
import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class NgrokCommandFactory {

    NgrokPropertiesHandler ngrokPropertiesHandler;

    public Command getExposeHttpCommand() {
        return NgrokConsoleCommand.ofKind(NgrokCommand.START_NGROK)
                .addArgument(LOG_LEVEL, ngrokPropertiesHandler.getLogLevel())
                .addArgument(LOG_FORMAT, ngrokPropertiesHandler.getLogFormat())
                .addArgument(LOG_TARGET, ngrokPropertiesHandler.getLogTarget())
                .addArgument(REGION, ngrokPropertiesHandler.getRegion())
                .addArgument(NONE);
    }

}
