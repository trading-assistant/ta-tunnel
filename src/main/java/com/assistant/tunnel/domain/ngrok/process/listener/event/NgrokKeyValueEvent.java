package com.assistant.tunnel.domain.ngrok.process.listener.event;

import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Opts;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Request;
import com.assistant.tunnel.domain.ngrok.process.listener.event.model.Response;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@FieldDefaults(level = PRIVATE)
@Parameters(separators = "=")
public class NgrokKeyValueEvent extends NgrokEvent {

    @Override
    @Parameter(names = "dur")
    public String getDur() {
        return super.getDur();
    }

    @Override
    @Parameter(names = "pg")
    public String getPg() {
        return super.getPg();
    }

    @Override
    @Parameter(names = "dur")
    public void setDur(String dur) {
        super.setDur(dur);
    }

    @Override
    @Parameter(names = "pg")
    public void setPg(String pg) {
        super.setPg(pg);
    }

    @Override
    @Parameter(names = "t")
    public String getTimestamp() {
        return super.getTimestamp();
    }

    @Override
    @Parameter(names = "lvl")
    public String getLvl() {
        return super.getLvl();
    }

    @Override
    @Parameter(names = "msg")
    public String getMsg() {
        return super.getMsg();
    }

    @Override
    @Parameter(names = "interval")
    public String getInterval() {
        return super.getInterval();
    }

    @Override
    @Parameter(names = "reqtype")
    public Integer getReqtype() {
        return super.getReqtype();
    }

    @Override
    @Parameter(names = "obj")
    public String getObj() {
        return super.getObj();
    }

    @Override
    @Parameter(names = "id")
    public String getId() {
        return super.getId();
    }

    @Override
    @Parameter(names = "clientid")
    public String getClientid() {
        return super.getClientid();
    }

    @Override
    @Parameter(names = "latency_ms")
    public String getLatency() {
        return super.getLatency();
    }

    @Override
    @Parameter(names = "secs")
    public Integer getSecs() {
        return super.getSecs();
    }

    @Override
    @Parameter(names = "name")
    public String getName() {
        return super.getName();
    }

    @Override
    @Parameter(names = "addr")
    public String getAddr() {
        return super.getAddr();
    }

    @Override
    @Parameter(names = "url")
    public String getUrl() {
        return super.getUrl();
    }

    @Override
    @Parameter(names = "proto")
    public String getProto() {
        return super.getProto();
    }

    @Override
    @Parameter(names = "err")
    public String getError() {
        return super.getError();
    }

    @Override
    @Parameter(names = "sid")
    public String getSid() {
        return super.getSid();
    }

    @Override
    @Parameter(names = "path")
    public String getPath() {
        return super.getPath();
    }

    @Override
    @Parameter(names = "ext")
    public String getExt() {
        return super.getExt();
    }

    @Override
    @Parameter(names = "req")
    public Request getReq() {
        return super.getReq();
    }

    @Override
    @Parameter(names = "resp")
    public Response getResp() {
        return super.getResp();
    }

    @Override
    @Parameter(names = "opts")
    public Opts getOpts() {
        return super.getOpts();
    }

    @Override
    @Parameter(names = "t")
    public void setTimestamp(String timestamp) {
        super.setTimestamp(timestamp);
    }

    @Override
    @Parameter(names = "lvl")
    public void setLvl(String lvl) {
        super.setLvl(lvl);
    }

    @Override
    @Parameter(names = "msg")
    public void setMsg(String msg) {
        super.setMsg(msg);
    }

    @Override
    @Parameter(names = "interval")
    public void setInterval(String interval) {
        super.setInterval(interval);
    }

    @Override
    @Parameter(names = "reqtype")
    public void setReqtype(Integer reqtype) {
        super.setReqtype(reqtype);
    }

    @Override
    @Parameter(names = "obj")
    public void setObj(String obj) {
        super.setObj(obj);
    }

    @Override
    @Parameter(names = "id")
    public void setId(String id) {
        super.setId(id);
    }

    @Override
    @Parameter(names = "clientid")
    public void setClientid(String clientid) {
        super.setClientid(clientid);
    }

    @Override
    @Parameter(names = "latency_ms")
    public void setLatency(String latency) {
        super.setLatency(latency);
    }

    @Override
    @Parameter(names = "secs")
    public void setSecs(Integer secs) {
        super.setSecs(secs);
    }

    @Override
    @Parameter(names = "name")
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    @Parameter(names = "addr")
    public void setAddr(String addr) {
        super.setAddr(addr);
    }

    @Override
    @Parameter(names = "url")
    public void setUrl(String url) {
        super.setUrl(url);
    }

    @Override
    @Parameter(names = "proto")
    public void setProto(String proto) {
        super.setProto(proto);
    }

    @Override
    @Parameter(names = "err")
    public void setError(String error) {
        super.setError(error);
    }

    @Override
    @Parameter(names = "sid")
    public void setSid(String sid) {
        super.setSid(sid);
    }

    @Override
    @Parameter(names = "path")
    public void setPath(String path) {
        super.setPath(path);
    }

    @Override
    @Parameter(names = "ext")
    public void setExt(String ext) {
        super.setExt(ext);
    }

    @Override
    @Parameter(names = "req")
    public void setReq(Request req) {
        super.setReq(req);
    }

    @Override
    @Parameter(names = "resp")
    public void setResp(Response resp) {
        super.setResp(resp);
    }

    @Override
    @Parameter(names = "opts")
    public void setOpts(Opts opts) {
        super.setOpts(opts);
    }
}
