package com.assistant.tunnel.domain.ngrok.process.listener.parser;

import com.assistant.tunnel.domain.ngrok.process.listener.event.NgrokEvent;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class EventLineParser {

    EventParserStrategy eventParserStrategy;

    public NgrokEvent parse(String eventLine) {
        return eventParserStrategy.parse(eventLine);
    }


}
