package com.assistant.tunnel.domain.ngrok.command;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum NgrokCommandArgument {

    LOG_LEVEL("-log-level"),
    LOG_FORMAT("-log-format"),
    LOG_TARGET("-log"),
    REGION("-region"),
    ALL("--all"),
    NONE("--none");

    String key;


}
