package com.assistant.tunnel.domain.ngrok.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "integration.ngrok")
public class NgrokIntegrationProperties {

    private String endpoint;
    private String tunnels;

    public String getTunnelsPath() {
        return endpoint + tunnels;
    }

    public String getTunnelPath(String tunnelName) {
        return getTunnelsPath() + tunnelName;
    }

}
