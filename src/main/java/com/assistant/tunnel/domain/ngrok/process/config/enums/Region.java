package com.assistant.tunnel.domain.ngrok.process.config.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public enum Region {

    UNITED_STATES("us", "United States"),
    EUROPE("eu", "Europe"),
    ASIA_PACIFIC("ap", "Asia/Pacific"),
    AUSTRALIA("au", "Australia"),
    SOUTH_AMERICA("sa", "South America"),
    JAPAN("jp", "Japan"),
    INDIA("in", "India");

    String sign;
    String name;

}
